/****************************************
 * Feedback
 ***************************************/

const Feedback = {};

Feedback.validate = function(target) {
  target.classList.remove('is-invalid');
  target.classList.add('is-valid');
};

Feedback.invalidate = function(target) {
  target.classList.remove('is-valid', 'is-invalid');
};

Feedback.error = function(target) {
  target.classList.remove('is-valid');
  target.classList.add('is-invalid');
};

//Implementaion de la fonction pour la progress-bar numero1
Feedback.progressbar = function(){
  let pro = (100/8)*$('.is-valid').length;
  pro = pro + "%";
  progressbar.style.width = pro;
}

//Implementaion de la fonction pour la progress-bar du mot de passe 
Feedback.progresse=(force)=>{
  let progress=document.querySelector('#barre');
  let pourcentage=(100/3)*(force+1);
  pourcentage=pourcentage+'%';
  progress.style.width=pourcentage;
};
