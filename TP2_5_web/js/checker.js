/****************************************
 * Checker functions
 ***************************************/

const Checker = {};
// pour le nom verifier
Checker.lastname = function() {
  const lastname = document.querySelector('#lastname');
  return lastname.value.length >= 2;
};

//pour le prenom verifier
Checker.firstname = function() {
  const firstname = document.querySelector('#firstname');
  return firstname.value.length >= 2;
};

//pour le mail verifier
Checker.email = function() {
  let email = document.querySelector('#email').value;
  let chaine = email.split('@');
  let domaine = email.split('.');
  if(chaine.length == 2 && chaine[0]!="" && chaine[1]!="" && domaine.length == 2 && domaine[0]!="" && domaine[1]!=""){
    return true;
  }else{
    return false;
  }
};

//pour la fonction isleapyear(year) pour l'annee bissextile
function isleapyear(year){
  let resultat = parseInt(year);
  if(resultat%400==0 || (resultat%4==0 && resultat%100!=0)){
    return true;
  }else{
    return false;
  }
};

//pour la verification de du jour
Checker.day = function(){
  let jour = document.querySelector('#day').value;
  let mois = document.querySelector('#month').value;
  let annee = document.querySelector('#year').value;
  if(jour<=31 && mois%2!=0 ){
    return true;
  }else if(jour<=30 && mois%2==0){
    return true;
  }else if(isleapyear(year)){
    return true;
  }else{
    return false;
  }  
};

//pour le mois en l'annee
Checker.month = Checker.day;
Checker.year = Checker.day;

//pour le mots de passe
Checker.password1 = function(){
  let motdepasse = document.querySelector('#password1').value;
  if(motdepasse.length>=6){
    return true;
  }else{
    return false;
  }
};

//pour la confirmation du mot_de_passe
Checker.password2 = function(){
  let confirme = document.querySelector('#password2').value;
  let motdepasse = document.querySelector('#password1').value;
  if(confirme == motdepasse){
    return true;
  }else{
    return false;
  }  
};

//fonction pour verifier la force du mot de passe
Checker.passStrength = function(){
  let motdepasse = document.querySelector('#password1').value;
  var regex = '((?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).{6,}';
  if(motdepasse.length < 6){
    return 0;
  }else if(motdepasse.match(regex)){
      return 2;
    }else{
    return 1;
  }
}




