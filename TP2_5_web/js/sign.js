/****************************************
 * Events
 ***************************************/
document
.querySelectorAll('input, select')
.forEach(el => {
  el.addEventListener('change',(evt)=> {
    let target = evt.target;
    if (Checker[target.id]()) {
      Feedback.validate(target);
    } else {
      Feedback.invalidate(target);
    }
    Feedback.progressbar();
  })
});

//pour le bouton valider(inscription)
document.querySelector('#sign').addEventListener('submit',(evt)=> {
  evt.preventDefault()
  let valide = document.querySelectorAll('.is-valid').length;
  let forme = document.querySelectorAll('input , select');

  if (valide === forme.length - 1) {
    //console.log('formulaire envoyer avec succes');
    document.querySelector('#sign').submit();
  } else {
    forme.forEach(el => {
      if (!(el.classList.contains('is-valid'))) {
        Feedback.error(el);
      }
    });
  }
});

//pour la force du mot de passe
let passeword=document.querySelector('#password1');
passeword.addEventListener('keyup',(evt)=>{
  let force=document.querySelector('#force');
  let progress=document.querySelector('#barre');
  let fonction=Checker.passStrength(passeword.value);

  if( passeword.value===""){

    force.classList.remove('text-success');
    force.classList.remove('text-warning');
    force.classList.remove('text-danger');
    force.textContent="Saisissez le mot de passe";
    progress.style.display='none';

  }else if(fonction===0){

    force.classList.remove('text-warning');
    force.classList.remove('text-success');
    force.classList.add('text-danger');
    force.textContent="Mot de passe invalide.";
    //pour la barre
    progress.classList.remove('bg-warning');
    progress.classList.remove('bg-success');
    progress.classList.add('bg-danger');
    progress.style.display='block';
    Feedback.progresse(fonction);

  } else if( fonction===2){

    force.classList.remove('text-danger')
    force.classList.remove('text-warning');
    force.textContent="Force du mot de passe excellent.";
    force.classList.add('text-success');
    //pour la barre
    progress.classList.remove('bg-danger');
    progress.classList.remove('bg-warning');
    progress.classList.add('bg-success');
    progress.style.display='block';
    Feedback.progresse(fonction);

  } else if(fonction===1){

    force.classList.remove('text-danger')
    force.classList.remove('text-success');
    force.textContent = " Force du mot de passe trop faible.";
    force.classList.add('text-warning');
    //pour la barre
    progress.classList.remove('bg-danger');
    progress.classList.remove('bg-success');
    progress.classList.add('bg-warning');
    progress.style.display='block';
    Feedback.progresse(fonction );
  }
});

