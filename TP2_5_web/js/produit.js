console.log('Hello world :) !');

//Declaration des variables pour les bottons 
let btnmoins = document.querySelector("#btnmoin");
let stocker = document.querySelector("#stock");
let btnplu = document.querySelector("#btnplus")

//mettre une action sur le button (-)
btnmoins.onclick = (event => {
    if(stocker.innerText > 1) {
        stocker.innerText = parseInt(stocker.innerText)-1;
    }

});

//mettre une action sur le button (+)
btnplu.onclick = (event => {
    if(stocker.innerText < 50) {
        stocker.innerText = parseInt(stocker.innerText)+1;
    }
});

// definition des miniatures sur les images
const miniatures = document.querySelectorAll(".miniature");
miniatures.forEach(Element => {
    Element.onclick = (event => {
        document.querySelector("#grdImage").src = event.target.src;
    });
});

//declation des mois, l'année et le format de l'heure
let now = new Date();
let annee = now.getFullYear();
let mois = now.getMonth() + 1;
let jour = now.getDay() + 1;

//definition des avis en utilisant une fonction
let Avis = document.querySelector("#avis");
function Commentaire(Avis) {
    this.Avis = Avis;
    new Date().getFullYear()
	this.date = jour+"/"+mois+"/"+annee;
}

$('#avis').keyup(function(event) {
	if (event.keyCode == 13) {
		var valeur = $('#avis').val() ;
		var com = new Commentaire(valeur);
		console.log(com);
		$('#avis').val('');
	}
});
