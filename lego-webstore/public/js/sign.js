/****************************************
 * Events
 ***************************************/
document
.querySelectorAll('input, select')
.forEach(el => {
  el.addEventListener('change',(evt)=> {
    let target = evt.target;
    if (Checker[target.id]()) {
      Feedback.validate(target);
    } else {
      Feedback.invalidate(target);
    }
    Feedback.progressbar();
  })
});

//pour le bouton valider(inscription)
document.querySelector('#sign').addEventListener('submit', (event) => {
  let last = document.querySelector('#lastname');
  let first = document.querySelector('#firstname');
  let days = document.querySelector('#day');
  let months = document.querySelector('#month');
  let years = document.querySelector('#year');
  let mails = document.querySelector('#email');
  let pass1 = document.querySelector('#password1');
  let pass2 = document.querySelector('#password2');
  document.querySelectorAll('.forme').forEach(el => {
    if (!(el.classList.contains('is-valid'))) {
      Feedback.error(el);
      if (last.value == "") {
        event.preventDefault();
      }else if (first.value == ""){
        event.preventDefault();
      }else if (days.value == "") {
        event.preventDefault();
      }else if (months.value == "") {
        event.preventDefault();
      }else if (years.value == "") {
        event.preventDefault();
      }else if (mails.value == "") {
        event.preventDefault();
      }else if (pass1.value == "") {
        event.preventDefault();
      }else if (pass2.value == "") {
        pass2.style.borderColor = 'red';
        event.preventDefault();
      }else {
        document.querySelector('#sign').submit();
      }
    }
  });
});

//pour la force du mot de passe
let passeword=document.querySelector('#password1');
passeword.addEventListener('keyup',(evt)=>{
  let force=document.querySelector('#force');
  let progress=document.querySelector('#barre');
  let fonction=Checker.passStrength(passeword.value);
  if( passeword.value == ""){
    force.classList.remove('text-success');
    force.classList.remove('text-warning');
    force.classList.remove('text-danger');
    force.textContent="Saisissez le mot de passe";
    progress.style.display='none';
  }else if(fonction == 0){
    force.classList.remove('text-warning');
    force.classList.remove('text-success');
    force.classList.add('text-danger');
    force.textContent="Mot de passe invalide.";
    //pour la barre
    progress.classList.remove('bg-warning');
    progress.classList.remove('bg-success');
    progress.classList.add('bg-danger');
    progress.style.display='block';
    Feedback.progresse(fonction);
  }else if ( fonction == 2) {
    force.classList.remove('text-danger')
    force.classList.remove('text-warning');
    force.textContent="Force du mot de passe excellent.";
    force.classList.add('text-success');
    //pour la barre
    progress.classList.remove('bg-danger');
    progress.classList.remove('bg-warning');
    progress.classList.add('bg-success');
    progress.style.display='block';
    Feedback.progresse(fonction);
  }else if (fonction == 1) {
    force.classList.remove('text-danger')
    force.classList.remove('text-success');
    force.textContent = " Force du mot de passe trop faible.";
    force.classList.add('text-warning');
    //pour la barre
    progress.classList.remove('bg-danger');
    progress.classList.remove('bg-success');
    progress.classList.add('bg-warning');
    progress.style.display='block';
    Feedback.progresse(fonction );
  }
});

