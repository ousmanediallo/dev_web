console.log('Hello world :) !');
console.log('Hello world :) !');

//definition des variables
let prixPanier = document.querySelector("#prix_panier");
let Prix = document.querySelectorAll(".prix");
let TVA = document.querySelector('#tva');
let  Total = document.querySelector('#total_panier');
let Supprimer = document.querySelectorAll(".btnSup");
let Quantite = document.querySelectorAll('.Qte');
let Listes = document.querySelectorAll('.liste_group');

//Calcul du Prix_Unitaire pour le Prix du Panier
let prix_unitaire = () => {
    let calcul=0;
    for (let i=0; i < Quantite.length; i++) {
        calcul += (parseInt(Quantite[i].innerHTML, 10).toFixed(2) * parseInt(Prix[i].innerHTML, 10).toFixed(2))
    }
    return calcul
};

//affichage du prix de panier
prixPanier.innerHTML = prix_unitaire()+ '€';

//Calcul du TVA
let  Prix_TVA;
Prix_TVA = () => {
    return (parseInt(prixPanier.innerHTML,10) * 0.15).toFixed(2)
}

//Affichage du TVA
TVA.innerHTML = Prix_TVA() + '€'

//Calcul et Affichage du Total du Panier
Total.innerHTML = (parseInt(prixPanier.innerHTML,10) + parseInt(TVA.innerHTML,10)).toFixed(2) + '€';

// Pour les botton Suppressions
for(let i=0; i<Supprimer.length; i++) {
    Supprimer[i].addEventListener('click',(evt) => {
        if(Quantite[i].innerHTML >  0) {
            Quantite[i].innerHTML--;
            prixPanier.innerHTML = prix_unitaire()+ '€';
            TVA.innerHTML = Prix_TVA() + '€'
            Total.innerHTML = (parseInt(prixPanier.innerHTML,10) + parseInt(TVA.innerHTML,10)).toFixed(2) + '€';
        }
        //Si la quantité est inferieure ou egal à 0, alors efface;
        if (Quantite[i].innerHTML <=0){
            Listes[i].remove();
        }
    });
}