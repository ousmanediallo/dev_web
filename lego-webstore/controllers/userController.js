const userManager = require('../models/userManager');

//pour la fonction sign_up
exports.sign_up = function(req, res){
   const status = req.query.status;
   res.render('layout_page.ejs',{title:"Inscription", page:"partials/inscription.ejs", status:status});
};

//pour la fonction register
exports.register = function(req, res) {
    const lastname = req.body.last;
    const firstname = req.body.first;
    const day = req.body.jour;
    const month = req.body.mois;
    const year = req.body.annee;
    const email = req.body.mail;
    const passwd  = req.body.passwd;
    const valider = req.body.valide;
    const inscription = userManager.insert_user(lastname, firstname, day+'/'+month+'/'+year, email, passwd);
    //redirection
    res.redirect('/inscription?status=inscription_success');
};

//pour la fonction sign_in
exports.sign_in = function(req, res) {
    const email = req.body.email;
    const passwd = req.body.passwd;
    const connect = userManager.get_user(email, passwd);
    if(connect){
        req.session.firstname = connect.firstname;
        res.redirect('/produits?status=success');
    }else{
        res.redirect('/produits?status=error');
    }
};

//pour la fonction sign_out
exports.sign_out = function(req, res) {
	const email = req.body.email;
	const passwd = req.body.passwd;
	const connexion = userManager.get_user(email, passwd);
    req.session.destroy(connexion)
    //redirection
	res.redirect('/produits?status=logout'); 
};