const productsManager = require('../models/productsManager');

//pour la liste de produits
exports.products_list= function(req, res) {
    const categories = productsManager.all_categories();
    const products = productsManager.all_products();
    const status = req.query.status;
    res.render("layout_page.ejs",{title:"Produits", page:"partials/products_list.ejs", categories:categories, products:products, status:status});
};

//pour le product_infos des produits
exports.product_infos = function(req, res) {
    const num = req.params.idproduct;
    const prod = productsManager.get_product(num);
    if(prod){
        res.render('layout_page.ejs',{title:"Fiche-Produit", page:"partials/product_infos.ejs", num, prod});
    }else{
        res.redirect('/produits');
    }     
};

//pour le panier losqu'on se connecte
exports.panier = function(req, res) {
    res.render('layout_page.ejs',{title:"Panier", page:"partials/panier.ejs"});
};

//pour la fonction products_list_search
exports.products_list_search = function() {
    res.send("Search");
};
