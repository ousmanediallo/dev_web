const express = require('express');
const router = express.Router();

const indexController = require('../controllers/indexController');
const productsController = require('../controllers/productsController');
const userController = require('../controllers/userController');

/* GET home page. */
router.get('/', indexController.index);

/*GET home page pour produit */
router.get('/produits', productsController.products_list);

/*page pour la fiche produit */
router.get('/produits/:idproduct', productsController.product_infos);

/*page pour le formulaire d'inscription */
router.get('/inscription', userController.sign_up);

/*pour la fonction register */
router.post('/inscription', userController.register);

/*pour la fonction connexion */
router.post('/connexion', userController.sign_in);

/*pour la deconnexion */
router.get('/deconnexion', userController.sign_out);

/*pour le panier */
router.get('/panier', productsController.panier);

/*products_list_search */
router.get('/produits/s', productsController.products_list_search);

module.exports = router;



