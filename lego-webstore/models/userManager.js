const manager = require('./manager');

//pour la fonction insert_user
exports.insert_user = function(lastname, firstname, birthdate, email, passwd) {
  const db = manager.connect();
  // TODO prepare SQL
  const sql = "INSERT INTO users (lastname, firstname, birthdate, email, passwd) values (?,?,?,?,?)";
  // TODO all results
  const results = db.prepare(sql).run(lastname, firstname, birthdate, email, passwd);
  // TODO close db
  db.close();
};

//pour bcrypt


//pour la fonction get_user
exports.get_user = function(email, passwd) {
  const db = manager.connect();
  // TODO prepare SQL
  const sql = "SELECT firstname FROM users WHERE users.email=? AND users.passwd=?";
  // TODO all results
  const results = db.prepare(sql).get(email, passwd);
  // TODO close db
  db.close();
  // TODO return results
  return results;
};