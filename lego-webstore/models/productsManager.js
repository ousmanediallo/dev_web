const manager = require('./manager');
//pour la fonction all_categories
exports.all_categories = function() {
  const db = manager.connect();
  // TODO prepare SQL
  const sql = 'SELECT name FROM categories';
  // TODO all results
  const results = db.prepare(sql).all();
  // TODO close db
  db.close();
  // TODO return results
  return results;
};

//pour la fonction all_products
exports.all_products = function() {
  const db = manager.connect();
  // TODO prepare SQL
  const sql = 'SELECT id, products.name as nom_produit, img0, price, categories.name as nom_categories FROM products INNER JOIN categories using (id)';
  // TODO all results
  const results = db.prepare(sql).all();
  // TODO close db
  db.close();
  // TODO return results
  return results;
};

//pour la fonction get_product
exports.get_product = function(idproduct) {
  const db = manager.connect();
  // TODO prepare SQL
  const sql='SELECT products.name as nom_produit,price,img0,img1,img2,img3,description,spec,categories.name as nom_categories FROM products INNER JOIN categories using (id) WHERE (id=?)'; 
  // TODO all results
  const resultats = db.prepare(sql).get(idproduct);
  // TODO close db
  db.close();
  // TODO return results
  return resultats;
};

//pour la fonction search_products(name)
exports.search_products = function(name) {
  const db = manager.connect();
  // TODO prepare SQL
  const sql='SELECT id, products.name as nom_produit, price, img0, categories.name as nom_categories FROM products INNER JOIN categories using (id) WHERE name LIKE "%name%"'; 
  // TODO all results
  const resultats = db.prepare(sql).all();
  // TODO close db
  db.close();
  // TODO return results
  return resultats;
};
